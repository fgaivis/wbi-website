//  ============================
//  = Full width video scripts =
//  ============================


jQuery(document).ready(function($) {
"use strict";


//  ===========================
//  = Define GLOBAL variables =
//  ===========================

var   win_height = $(window).height(),
      body = $('body'),
      current_width = $(window).width(),
      windowTop = $(window).scrollTop(),

//  ==================
//  = Menu variables =
//  ==================
      sticky = $("#sticky"),
      small_logo = $(".small-logo"),
      navigation_height = $('#sticky').height(),
      offset = sticky.offset(),
      stickyTop = offset.top,

      topMenu = $(".top-nav-full"),
      topMenuHeight = topMenu.outerHeight()+20,
      menuItems = topMenu.find("a"),
      not_sticky = $('#not-sticky'),
      scroll_up = $('#scroll-up'),
      header_button = $('.header-button'),
      revolSlider = $('.banner'),
      body_preloader = $('#body-preloader'), //loading screen selector

      //small menu on top
      top_small_menu = $('#top-small-menu'),
      top_logo = $('.top-logo'),
      top_logo_bg = $('.top-logo-bg'),


      sec_pcbig = $('.pc-big')




//  ==================
//  = Loading screen =
//  ==================
window.addEventListener('DOMContentLoaded', function() {
  body_preloader.fadeOut('slow');
});


//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////





//  =====================
//  = Revolution Slider =
//  =====================

revolSlider.revolution({
         delay:9000,
         startwidth:960,
         startheight:600,
         autoHeight:"on",
         fullScreenAlignForce:"on",

         onHoverStop:"off",

         thumbWidth:100,
         thumbHeight:50,
         thumbAmount:3,

         hideThumbsOnMobile:"off",
         hideBulletsOnMobile:"off",
         hideArrowsOnMobile:"on",
         hideThumbsUnderResoluition:0,

         hideThumbs:1,

         navigationType:"none",
         navigationArrows:"solo",
         navigationStyle:"round",

         navigationHAlign:"center",
         navigationVAlign:"bottom",
         navigationHOffset:30,
         navigationVOffset:30,

         soloArrowLeftHalign:"left",
         soloArrowLeftValign:"center",
         soloArrowLeftHOffset:20,
         soloArrowLeftVOffset:-30,

         soloArrowRightHalign:"right",
         soloArrowRightValign:"center",
         soloArrowRightHOffset:20,
         soloArrowRightVOffset:-30,


         touchenabled:"on",

         stopAtSlide:-1,
         stopAfterLoops:-1,
         hideCaptionAtLimit:0,
         hideAllCaptionAtLilmit:0,
         hideSliderAtLimit:0,

         dottedOverlay:"none",

         fullWidth:"on",
         forceFullWidth:"on",
         fullScreen:"on",
         // fullScreenOffsetContainer:".bannercontainer",
         shadow:0,
         videoJsPath:"scripts/rs-plugin/videojs/",

      });

//  =========================
//  = end Revolution Slider =
//  =========================




//  ==========
//  = responsive Navigation =
//  ==========

$().jetmenu({
  speed: 200,
  hideClickOut: true,
});




//  =================
//  = Appear [load] =
//  =================

      sec_pcbig.find('.macbook-top').appear();

      appearPcBig();


//  ==============================
//  = Appear [PC Big] =
//  ==============================
function appearPcBig() {
var current_data_perc_pc = 0;

$(document.body).on('appear', '.pc-big .macbook-top', function(e, $affected) {
  if (current_data_perc_pc == 0) {
      sec_pcbig.find('.macbook-top').addClass('translate-default');
      current_data_perc_pc = 1;
  };
});
}



//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

//  ===============
//  = PrettyPhoto =
//  ===============

$("a[data-rel^='prettyPhoto']").prettyPhoto({
  show_title: false,
  social_tools: false,
  // theme: 'light_square',
  deeplinking: false,
  theme: 'light_square',
});
//  =====================
//  = [end] PrettyPhoto =
//  =====================

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////




//  =====================
//  = Change size of menu on scroll =
//  =====================

function MenuSizeChange(){
    var windowTop = $(window).scrollTop();

    if (windowTop > 1 && !(sticky.hasClass('sticky-small'))) {
      sticky.removeClass('sticky-default');
      $('#not-sticky').removeClass('sticky-default');
      sticky.addClass('sticky-small');
      $('#not-sticky').addClass('sticky-small');
      $('#scroll-up').removeClass('translate-default');

      $('#top-small-menu').css({'height':'30px','line-height':'30px'});
      $('.top-logo').css('height','30px');
      $('.top-logo-bg').css('line-height','30px');




    } else if (windowTop <= 1 && sticky.hasClass('sticky-small')) {
      sticky.removeClass('sticky-small');
      $('#not-sticky').removeClass('sticky-small');
      sticky.addClass('sticky-default');
      $('#not-sticky').addClass('sticky-default');
      $('#scroll-up').removeClass('translate-default');



      $('#top-small-menu').css({'height':'50px','line-height':'50px'});
      $('.top-logo').css('height','50px');
      $('.top-logo-bg').css('line-height','60px');

      offset = sticky.offset();
      stickyTop = offset.top;


    }


    if (windowTop > stickyTop && sticky.hasClass('sticky-small')) {
      sticky.removeClass('sticky-small');
      $('#not-sticky').removeClass('sticky-small');
      $('#not-sticky').addClass('sticky-small-second');
      $('#scroll-up').addClass('translate-default');
      sticky.addClass('sticky-small-second');
      small_logo.addClass('opacity-up');



    } else if (windowTop < stickyTop) {
      sticky.removeClass('sticky-small-second');
      $('#not-sticky').removeClass('sticky-small-second');
      offset = sticky.offset();
      stickyTop = offset.top;
      small_logo.removeClass('opacity-up');

    }

}

//  ===========================
//  = [end] Change size of menu on scroll =
//  ===========================


//  ===========
//  = FitVids =
//  ===========
$(".media-holder").fitVids();
$(".container").fitVids();
//  =================
//  = [end] FitVids =
//  =================


//  =====================
//  = Stick Menu on Top =
//  =====================

function StickMenu(){
  windowTop = $(window).scrollTop();

  if (windowTop > stickyTop) {
    sticky.css({position:'fixed'});
  } else {
    sticky.css({position:'absolute'});
  }
}

//  ===========================
//  = [end] Stick Menu on Top =
//  ===========================


//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

//  =======================================
//  = Toggle more videos in video section =
//  =======================================
$('.video-section .button-more a').click(function() {
    $('.additional-videos').slideToggle('800','easeInOutCubic');
    $(this).children('p').text($(this).children('p').text() == 'Show other videos' ? 'Hide other videos' : 'Show other videos');
    return false;
});
//  =============================================
//  = [end] Toggle more videos in video section =
//  =============================================


//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

//  ============
//  = SlabText =
//  ============
$("#top-content h1").slabText({
  'maxFontSize': 80,
  // 'viewportBreakpoint' : 900,
  'minCharsPerLine' : 10
});


$("#footer-section h1").slabText({
  'maxFontSize': 80,
  // 'viewportBreakpoint' : 900,
  'minCharsPerLine' : 10
});
//  ==================
//  = [end] SlabText =
//  ==================

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////


//  =================
//  = Scroll to top =
//  =================

$('#scroll-up').click(function(){
    $("html, body").stop().animate({ scrollTop: 0 }, 2000,'easeInOutQuart');
    return false;
});

//  =======================
//  = [end] Scroll to top =
//  =======================



//  ======================
//  = Menu Scroll effect =
//  ======================

//Author: Marcus Ekwall
//URL: http://stackoverflow.com/users/358556/marcus-ekwall

// Cache selectors
var lastId,
    topMenu = $(".top-nav-full"),
    topMenuHeight = topMenu.outerHeight()+20,
    // All list items
    menuItems = topMenu.find("a"),
    current_width = $(window).width()

    // Anchors corresponding to menu items
    var scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });


// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href");


    offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;

  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 1500, 'easeInOutQuart');
  e.preventDefault();
});
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////





//  ================================
//  = Call events on window scroll =
//  ================================
$(window).on('scroll', function() {

  StickMenu();
  MenuSizeChange();


   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;

   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";

   if (lastId !== id) {

       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href=#"+id+"]").parent().addClass("active");
   }

});


//  ================================
//  = Load events on window resize =
//  ================================
$(window).resize(function() {
    // responsiveColumns();


//  ======================
//  = Menu Scroll effect =
//  ======================

//Author: Marcus Ekwall
//URL: http://stackoverflow.com/users/358556/marcus-ekwall

// Cache selectors
var lastId,
    topMenu = $(".top-nav-full"),
    topMenuHeight = topMenu.outerHeight()+20,
    // All list items
    menuItems = topMenu.find("a"),
    current_width = $(window).width()

    // Anchors corresponding to menu items
    var scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });


// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href");


    offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;

  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 1500, 'easeInOutQuart');
  e.preventDefault();
});


});
//  ======================================
//  = [end] Load events on window resize =
//  ======================================

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

});


//  ============================================================
//  = Load functions after page and other functions are loaded =
//  ============================================================
$(window).load(function() {


});